variable "project_id" {
  type        = string
  description = "The ID of the GCP project."
}
variable "region" {
  type        = string
  description = "The GCP region where resources will be created."
}
variable "zone" {
  type        = string
  description = "The GCP zone where resources will be created."
}
variable "name" {
  type        = string
  description = "Name of instance."
}
variable "machine_type" {
  type        = string
  description = "machine type."
}
variable "bucket_name" {
  type        = string
  description = "bucket name."
}