
 resource "google_storage_bucket" "auto-expire" {
  name          = "santosh_gcp_bucket_iac_new"
  location      = "US"
  force_destroy = true

  public_access_prevention = "enforced"
}
                 
resource "google_compute_instance" "example" {
  name         = "terraform"
  machine_type = "n1-standard-1"
  zone         = "us-east1-b"
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  network_interface {
    network = "default"
    access_config {}
  }
}
resource "google_compute_instance" "sample" {
  name         = var.name
  machine_type = var.machine_type
  zone         = var.zone
  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-10"
    }
  }
  network_interface {
    network = "default"
    access_config {}
  }
}
resource "google_storage_bucket" "santubikl" {
  name          = var.bucket_name
  location      = var.region
  force_destroy = true

  public_access_prevention = "enforced"
}

resource "google_container_cluster" "demo" {
  name = "demo-cluster"
  location = "us-east1-b"

  node_pool {
    name = "default"
    node_count = 1
    node_config {
      machine_type = "e2-micro"
      disk_size_gb = 10

    }
  }
}