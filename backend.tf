terraform {
    required_version = "~>1.4.2"
    backend "gcs" {
         credentials = "./cred/serviceaccount.json"
         bucket      = "santosh_gcp_bucket_iacc"
    }
}